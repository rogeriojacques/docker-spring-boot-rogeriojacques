FROM openjdk:12
ADD target/docker-spring-boot-rogeriojacques.jar docker-spring-boot-rogeriojacques.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-rogeriojacques.jar"]
